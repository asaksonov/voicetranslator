//
//  File.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 30.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

import UIKit
class ArchiveUtil {
    
    private static let ImageKey = "ImageKey"
    
    static func loadImage() -> [ImageModelProtocol]? {
        do {
            guard let userDefaultData = UserDefaults.standard.object(forKey: ImageKey) as? NSData else { return nil }
            let data = Data(referencing:userDefaultData)
            if let loadedMeals = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [UIImage] {
                var ImageArray = [ImageModelProtocol]()
                for img in loadedMeals {
                    ImageArray.append(ImageModel(image: img, isLoad: true))
                }

                return ImageArray
            }
        } catch { }
        return nil
    }
    
    static func saveImage(image : [ImageModelProtocol]?) {
        guard let images = image, !images.isEmpty else { return }
        var ImageArray = [UIImage]()
        for img in images {
            ImageArray.append(img.image)
        }
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject:ImageArray, requiringSecureCoding:false)
            UserDefaults.standard.set(data, forKey: ImageKey)
        } catch {}
    }
}
