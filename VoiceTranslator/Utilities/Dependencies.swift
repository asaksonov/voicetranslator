//
//  Dependencies.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

class Dependencies {

    static let sharedDependencies = Dependencies()

    let cache: Storable

    private init() {
        cache = ApplicationCache()
    }
}
