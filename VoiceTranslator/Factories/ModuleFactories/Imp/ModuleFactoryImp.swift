//
//  ModuleFactoryImp.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

final class ModuleFactoryImp: ModuleFactoryList {
    
    // MARK: - LoaderModuleFactory
    
    func makeMainModule() -> MainViewInput & MainViewOutput {
        return MainViewController.controllerFromStoryboard(.main)
    }
    
    func makeLoaderModule() -> LoaderViewOutput {
        return LoaderViewController.controllerFromStoryboard(.main)
    }
}
