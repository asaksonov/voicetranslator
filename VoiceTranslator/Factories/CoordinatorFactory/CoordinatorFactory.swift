//
//  CoordinatorFactory.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

protocol CoordinatorFactory {
    func makeLoaderCoordinator(router: Router) -> Coordinatable
}
