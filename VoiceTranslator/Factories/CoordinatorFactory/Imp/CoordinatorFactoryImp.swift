//
//  CoordinatorFactoryImp.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

final class CoordinatorFactoryImp: CoordinatorFactory {

    func makeLoaderCoordinator(router: Router) -> Coordinatable {
        return MainCoordinator(with: ModuleFactoryImp(), router: router)
    }
    
    // MARK: - Private methods

    private func router(_ navController: UINavigationController?) -> Router {
        return RouterImp(rootController: navigationController(navController))
    }

    private func navigationController(_ navController: UINavigationController?) -> UINavigationController {
        if let navController = navController {
            return navController
        } else {
            return CustomNavigationController.controllerFromStoryboard(.main)
        }
    }

}
