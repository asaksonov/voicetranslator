//
//  LoaderCoordinator.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

class MainCoordinator: BaseCoordinator {
    
    private let factory: LoaderModuleFactory
    private let router: Router
    private let memoryCache: Storable = Dependencies.sharedDependencies.cache

    private var mainModule: (MainViewInput & MainViewOutput)?
    
    init(with factory: LoaderModuleFactory, router: Router) {
        self.factory = factory
        self.router = router
    }
    
    override func start() {
        showLoaderModule()
        syncData()
    }
    
    // MARK: - Private Methods
    
    private func showLoaderModule() {
        let loaderModule = factory.makeLoaderModule()
        loaderModule.onStertButton = { [weak self] in
            self?.showMainModule()
        }
        router.setRootModule(loaderModule)
    }

    private func showMainModule() {
        let mainModule = factory.makeMainModule()
        router.push(mainModule)
    }

    private func syncData() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.finishFlow?()
        }
    }
}
