//
//  LoaderViewOutput.swift
//  TaskTestCFT
//
//  Created by Mac Mini on 04.08.2019.
//  Copyright © 2019 Mac Mini. All rights reserved.
//

protocol LoaderViewOutput: BaseView {
    var onStertButton: Action? { get set }
}
