//
//  HeaderView.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

enum ButtonType: Int {
    case turn
    case gamma
    case reflect
}

import UIKit

class MainTableHeaderView: PrimaryView {
    
    // MARK: - IBOutlets

    @IBOutlet private var turnButton: UIButton! {
        didSet {
            turnButton = turnButton.addDesignStyle()
            turnButton.setTitle("Alert.Button.Turn".localized, for: .normal)
        }
    }
    @IBOutlet private var gammaButton: UIButton! {
        didSet {
            gammaButton = gammaButton.addDesignStyle()
            gammaButton.setTitle("Alert.Button.Convert".localized, for: .normal)
        }
    }
    @IBOutlet private var reflectButton: UIButton! {
        didSet {
            reflectButton = reflectButton.addDesignStyle()
            reflectButton.setTitle("Alert.Button.Reflect".localized, for: .normal)
        }
    }
      
    var setImage: Action?
    var addNewImage: ImageAction?

    @IBOutlet private var imageView: UIImageView! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            self.imageView.addGestureRecognizer(tap)
            self.imageView = imageView.addDesignStyle()
        }
    }
    
    // MARK: - Life cycle

    @IBAction func correctImage(_ sender: UIButton) {
        guard let image = imageView.image else {
            setImage?()
            return
        }
        
        switch sender.tag {
        case ButtonType.turn.rawValue: self.addNewImage?(image.imageRotatedByDegrees(deg: 90.0))
        case ButtonType.gamma.rawValue: self.addNewImage?(image.setBlackAndWhiteImage())
        case ButtonType.reflect.rawValue: self.addNewImage?(image.flippedImage())
        default: break
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.layer.cornerRadius = 20.0
    }
    
    // MARK: - Private methods

    @objc private func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        setImage?()
    }
    
    // MARK: - Public methods

    func addImage(image: UIImage) {
        self.imageView.image = image
    }
}
