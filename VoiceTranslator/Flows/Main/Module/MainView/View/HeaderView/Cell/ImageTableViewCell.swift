//
//  ImageCellView.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell, CellInterface {
   
    // MARK: - IBOutlets

    @IBOutlet private var imageViewCell: UIImageView! {
        didSet {
            self.imageViewCell = imageViewCell.addDesignStyle()
        }
    }
    var indexPathRowAction: IndexBlock?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 20.0
    }
    
    // MARK: - configure

    func configure(image: ImageModelProtocol, indexPathRow: Int) {
        let randomInt = Int.random(in: 5...30)
        if !image.isLoad {
            ProgressView().showProgress(view: self, timeInt: randomInt) { [weak self] in
                self?.imageViewCell?.image = image.image
                self?.indexPathRowAction?(indexPathRow)
            }
        } else {
            self.imageViewCell?.image = image.image
        }
    }
}
