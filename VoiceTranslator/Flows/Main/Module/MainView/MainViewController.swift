//
//  LoaderViewController.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

final class MainViewController: BaseViewController, MainViewInput, MainViewOutput {
    
    // MARK: - Outlets

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var headerTableView: MainTableHeaderView!

    private var tableViewItemArray: Array<ImageModelProtocol> = ArchiveUtil.loadImage() ?? [] {
        didSet {
            ArchiveUtil.saveImage(image: tableViewItemArray)
        }
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Actions

    // MARK: - Private Methods

    private func setupUI() {
        headerTableView.setImage = { [weak self] in
            self?.thisIsTheFunctionWeAreCalling()
        }
        
        headerTableView.addNewImage = { [weak self] (image) in
            guard let image = image else { return }
            self?.tableViewItemArray.insert(ImageModel(image: image), at: 0)
            self?.tableView.beginUpdates()
            self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            self?.tableView.endUpdates()
        }
        
        tableView.delegate  = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageTableViewCell")

    }

}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewItemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath)
            as? ImageTableViewCell else {
                fatalError("Cell is not of kind \(ImageTableViewCell.nameOfClass)")
        }
        cell.configure(image: self.tableViewItemArray[indexPath.row], indexPathRow: indexPath.row)
        cell.indexPathRowAction = { [weak self] (index) in
            self?.self.tableViewItemArray[index].setIsLoad()
        }
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let image = self.tableViewItemArray[indexPath.row]
        self.createAlert(array: [(title: "Alert.Button.Delete".localized, action: { [weak self] in
            self?.deleteCell(indexPath: indexPath)
        }), (title: "Alert.Button.Use".localized, action: { [weak self] in
            self?.headerTableView.addImage(image: image.image)
            self?.deleteCell(indexPath: indexPath)
        }), (title: "Alert.Button.Save".localized, action: { [weak self] in
            UIImageWriteToSavedPhotosAlbum(image.image, self, #selector(self?.image(_:didFinishSavingWithError:contextInfo:)), nil)
            self?.deleteCell(indexPath: indexPath)
        }), (title: "Alert.Button.Cancel".localized, action: {})])
    }
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showAlertWith(title: "Alert.Button.Saved.Error".localized, message: error.localizedDescription)
        } else {
            showAlertWith(title: "Alert.Button.Saved".localized, message: "Alert.Button.Saved.Message".localized)
        }
    }

    private func deleteCell(indexPath: IndexPath) {
        self.tableViewItemArray.remove(at: indexPath.row)
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
        self.tableView.endUpdates()
    }
}

extension MainViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func thisIsTheFunctionWeAreCalling() {
        let camera = DSCameraHandler(delegate_: self)
        self.createAlert(array: [(title: "Alert.Button.TakePhoto".localized, action: { [weak self] in
            camera.getCameraOn(self!, canEdit: true)
        }), (title: "Alert.Button.PhotoLibrary".localized, action: { [weak self] in
            camera.getPhotoLibraryOn(self!, canEdit: true)
        }), (title: "Alert.Button.DownloadForUrl".localized, action: { [weak self] in
            self?.createURLAlert(title: "Alert.Button.EnterURL".localized, placeholder: "Alert.Button.URL".localized, titleButton: "Alert.Button.Ready".localized, action: { [weak self] (urlString) in
                guard let url = URL(string: urlString) else { return }
                let progressView = ProgressView()
                
                progressView.showActivityIndicator(view: self!.view)
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url)
                    DispatchQueue.main.async { [weak self] in
                        progressView.hideActivityIndicator(view: self!.view)
                        guard let data = data, let image = UIImage(data: data) else { return }
                        self?.headerTableView.addImage(image:image)
                    }
                }
            })
        }), (title: "Alert.Button.Cancel".localized, action: {})])
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        self.headerTableView.addImage(image: image)
        dismiss(animated: true, completion: nil)
    }
}
