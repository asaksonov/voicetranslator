//
//  Presentable.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

protocol Presentable {
    func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {

    func toPresent() -> UIViewController? {
        return self
    }
}

protocol ViewIsBeingShowable {
    func isBeingShowableViewForModule(module: Presentable) -> Bool
}
