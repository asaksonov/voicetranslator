//
//  CustomNavigationController.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

final class CustomNavigationController: UINavigationController {

    // MARK: - Overriden methods

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override func awakeFromNib() {
        configureUI()
    }
    
    // MARK: - Private methods

    private func configureUI() {
        navigationBar.removeFromSuperview()
    }
}
