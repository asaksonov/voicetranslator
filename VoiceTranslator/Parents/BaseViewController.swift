//
//  BaseViewController.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

class BaseViewController: UIViewController {

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleTapBackground()
    }

    deinit {
        print("\(self.nameOfClass) deinit")
    }
    
    // MARK: - Public methods
    
    func handleTapBackground() {
        let tapBackground = UITapGestureRecognizer()
        tapBackground.cancelsTouchesInView = false
        view.addGestureRecognizer(tapBackground)
    }
}
