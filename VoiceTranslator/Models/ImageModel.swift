//
//  ImageModelProtocol.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

struct ImageModel: ImageModelProtocol {
    var image: UIImage
    var isLoad: Bool 

    init(image: UIImage, isLoad: Bool = false) {
        self.image = image
        self.isLoad = isLoad
    }
    
    mutating func setIsLoad() {
        self.isLoad = true
    }
}
