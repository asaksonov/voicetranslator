//
//  UIViewControllerExtension.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

extension UIViewController {
    private static func instantiateControllerInStoryboard<T: UIViewController>(_ storyboard: UIStoryboard, identifier: String) -> T {
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }

    static func controllerInStoryboard(_ storyboard: UIStoryboard, identifier: String) -> Self {
        return instantiateControllerInStoryboard(storyboard, identifier: identifier)
    }

    static func controllerInStoryboard(_ storyboard: UIStoryboard) -> Self {
        return controllerInStoryboard(storyboard, identifier: nameOfClass)
    }

    static func controllerFromStoryboard(_ storyboard: Storyboards) -> Self {
        return controllerInStoryboard(UIStoryboard(name: storyboard.rawValue, bundle: nil), identifier: nameOfClass)
    }
    
    func createAlert(array:[(title: String, action: Action?)]) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        for alertAction in array {
            alert.addAction(UIAlertAction(title: alertAction.title, style: .default) { (alert : UIAlertAction!) in
                alertAction.action?()
            })
            alert.view.tintColor = UIColor.red.withAlphaComponent(0.7)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func createURLAlert(title: String, placeholder: String, titleButton: String, action: InsertTextBlock?) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.popoverPresentationController?.sourceView = self.view
        alert.addAction(UIAlertAction(title: titleButton, style: .default) { (aciton) in
            guard let text = alert.textFields?.first?.text else { return }
            action?(text)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { (alert : UIAlertAction!) in })
        alert.addTextField { (textField) in
            textField.placeholder = placeholder
        }
        self.present(alert, animated: true, completion: nil)
    }
}
