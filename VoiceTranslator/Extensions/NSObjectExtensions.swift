//
//  NSObjectExtensions.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit
extension NSObject {
    // swiftlint:disable missing_docs
    public class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }

    // swiftlint:disable missing_docs
    public var nameOfClass: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }

    static var nibName: String {
        return String(describing: self)
    }

}
