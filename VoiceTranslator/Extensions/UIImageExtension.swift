//
//  UIImageExtension.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit
extension UIImage {
    func imageRotatedByDegrees(deg degrees: CGFloat) -> UIImage? {
        let size = self.size
        UIGraphicsBeginImageContext(size)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        bitmap.translateBy(x: size.width / 2, y: size.height / 2)
        bitmap.rotate(by: (degrees * CGFloat(Double.pi / 180)))
        bitmap.scaleBy(x: 1.0, y: -1.0)
        let origin = CGPoint(x: -size.width / 2, y: -size.width / 2)
        if let image = self.cgImage {
        bitmap.draw(image, in: CGRect(origin: origin, size: size))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
        }
        return nil
    }
    
    func setBlackAndWhiteImage() -> UIImage? {
            guard let ciImage = CIImage(image: self, options: nil) else { return self }
            let paramsColor: [String: AnyObject] = [kCIInputBrightnessKey: NSNumber(value: 0.0), kCIInputContrastKey: NSNumber(value: 1.0), kCIInputSaturationKey: NSNumber(value: 0.0)]
            let grayscale = ciImage.applyingFilter("CIColorControls", parameters: paramsColor)
            guard let processedCGImage = CIContext().createCGImage(grayscale, from: grayscale.extent) else { return self }
            return UIImage(cgImage: processedCGImage, scale: scale, orientation: imageOrientation)
    }

    func flippedImage() -> UIImage? {
        return self.withHorizontallyFlippedOrientation()
    }
}

extension UIImageView {
    func addDesignStyle() -> UIImageView {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: -2.0, height: 5.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 2.0
        
        self.layer.cornerRadius = 15.0
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 2.0
        self.layer.masksToBounds = false
        return self
    }
}
