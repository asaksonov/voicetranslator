//
//  UIButtonExtension.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit
extension UIButton {
    func addDesignStyle() -> UIButton {
        
        self.titleLabel?.font = UIFont(name:"Avenir-Light", size: 18.0)
        self.backgroundColor = UIColor.red.withAlphaComponent(0.7)
        self.setTitleColor(UIColor.white, for: .normal)
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: -2.0, height: 5.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 2.0
        
        self.layer.cornerRadius = 15.0
        
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 2.0
        
        return self
    }
}
