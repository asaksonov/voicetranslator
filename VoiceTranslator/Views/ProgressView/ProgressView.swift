//
//  ProgressView.swift
//  VoiceTranslator
//
//  Created by Alexandr Saxonov on 23.08.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.

import UIKit

final class ProgressView: PrimaryView {

    // MARK: - Outlets
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private var progressView: UIProgressView!
    let progress = Progress(totalUnitCount: 10)
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        progressView.transform = progressView.transform.scaledBy(x: 1, y: 2)
    }
    
    // MARK: - Public Methods

    func showProgress(view: UIView, timeInt: Int, action: Action? ) {
        self.frame.size.height = view.frame.size.height
        self.frame.size.width = UIScreen.main.bounds.size.width

        view.addSubview(self)
        progressView.alpha = 1.0
        self.alpha = 0.7
        progress.totalUnitCount = Int64(timeInt)
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            guard self.progress.isFinished == false else {
                self.alpha = 0.0
                self.progressView.alpha = 0.0
                timer.invalidate()
                action?()
                return
            }
            self.progress.completedUnitCount += 1
            let progressFloat = Float(self.progress.fractionCompleted)
            self.progressView.setProgress(progressFloat, animated: true)
        }
    }
    
    func showActivityIndicator(view: UIView) {
        self.frame = view.frame
        view.addSubview(self)
        self.alpha = 0.7
        activityIndicatorView.alpha = 1.0
    }
    
    func hideActivityIndicator(view: UIView) {
        self.alpha = 0.0
        activityIndicatorView.alpha = 0.0
    }
}
